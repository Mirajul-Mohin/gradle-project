package net.therap.gradleProject.domain;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author mirajul.mohin
 * @since 2/5/20
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Size {

    int min() default 1;

    int max() default 100;

    String message() default "Length must be {min} - {max}";
}
