package net.therap.gradleProject.app;

import net.therap.gradleProject.domain.Person;
import net.therap.gradleProject.domain.ValidationError;
import net.therap.gradleProject.service.AnnotatedValidator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mirajul.mohin
 * @since 2/5/20
 */
public class Main {

    public static void main(String[] args) {

        Person p = new Person("Hello There", 5);

        List<ValidationError> errors = new ArrayList<ValidationError>();

        AnnotatedValidator validator = new AnnotatedValidator();
        validator.validate(p, errors);
        validator.print(errors);
    }
}
