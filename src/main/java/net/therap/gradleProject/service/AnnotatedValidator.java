package net.therap.gradleProject.service;

import net.therap.gradleProject.domain.Person;
import net.therap.gradleProject.domain.Size;
import net.therap.gradleProject.domain.ValidationError;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

/**
 * @author mirajul.mohin
 * @since 2/9/20
 */
public class AnnotatedValidator {

    public static void validate(Person person, List<ValidationError> errors) {

        Class cls = person.getClass();
        Field[] fields = cls.getDeclaredFields();

        for (int index = 0; index < fields.length; index++) {

            Annotation annotation = fields[index].getAnnotation(Size.class);

            if (annotation instanceof Size) {
                Size myAnnotation = (Size) annotation;

                Object obj = fields[index].getType();

                if (obj.equals(String.class)) {

                    if (person.getName().length() > myAnnotation.max()) {
                        String str = myAnnotation.message().replace("{min}", String.valueOf(myAnnotation.min()));
                        str = str.replace("{max}", String.valueOf(myAnnotation.max()));
                        str = fields[index].getName() + " (String) : " + str;
                        errors.add(new ValidationError(str));
                    }
                } else {

                    if (person.getAge() < myAnnotation.min() || person.getAge() > myAnnotation.max()) {
                        String str = myAnnotation.message().replace("{min}", String.valueOf(myAnnotation.min()));
                        str = fields[index].getName() + " (" + obj + ") : " + str;
                        errors.add(new ValidationError(str));
                    }
                }
            }
        }
    }

    public static void print(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            System.out.println(error.getError());
        }
    }
}
